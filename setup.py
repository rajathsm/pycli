from setuptools import setup, find_packages
 
 
setup(
  name='pyCli',
  version='0.0.1',
  description='cmd tool to upload files to firebase storage',  
  author='rajath',
  author_email='nagarajathsm@gmail.com',
  keywords='calculator',
  
        entry_points ={ 
            'console_scripts': [ 
                'cli = cli:main'
            ] 
        },  
  
  install_requires=['pyrebase','fire'] 
)
